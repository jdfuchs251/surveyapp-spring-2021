﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using JDF.SurveyMaker.BL;
using JDF.SurveyMaker.BL.Models;

namespace JDF.SurveyMaker.BL.Test
{
    [TestClass]
    public class utActivation
    {
        [TestMethod]
        public void InsertTest()
        {
            var task1 = QuestionManager.Load();
            task1.Wait();
            Guid questionId = task1.Result.FirstOrDefault().Id;

            Activation activation = new Activation
            {
                QuestionId = questionId,
                StartDate = new DateTime(2021, 5, 2), // Test will fail if dates overlap an existing acivation in the DB
                EndDate = new DateTime(2021, 5, 2),
                ActivationCode = "test77"
            };

            var task2 = ActivationManager.Insert(activation, true);
            task2.Wait();
            Assert.IsTrue(task2.Result > 0);
        }

        [TestMethod]
        public void UpdateTest()
        {
            var task1 = QuestionManager.LoadByActivationCode("test01", new DateTime(2021, 2, 15));
            task1.Wait();
            Activation activation = task1.Result.Activations.FirstOrDefault();

            activation.ActivationCode = "test10";

            var task2 = ActivationManager.Update(activation, true);
            task2.Wait();
            Assert.IsTrue(task2.Result > 0);
        }

        [TestMethod]
        public void DeleteTest()
        {
            var task1 = QuestionManager.Load();
            task1.Wait();
            Guid questionId = task1.Result.FirstOrDefault().Id;

            Activation activation = new Activation
            {
                QuestionId = questionId,
                StartDate = new DateTime(2021, 4, 1),
                EndDate = new DateTime(2021, 4, 15),
                ActivationCode = "test04"
            };

            var task2 = ActivationManager.Insert(activation); // Do an insert without a rollback to get test data to delete
            task2.Wait();

            var task3 = ActivationManager.Delete(activation.Id); // Delete the activation we just inserted, also with no rollback
            task3.Wait();
            Assert.IsTrue(task3.Result > 0);
        }
    }
}
