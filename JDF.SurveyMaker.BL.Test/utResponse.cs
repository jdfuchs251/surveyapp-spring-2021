﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using JDF.SurveyMaker.BL;
using JDF.SurveyMaker.BL.Models;

namespace JDF.SurveyMaker.BL.Test
{
    [TestClass]
    public class utResponse
    {
        [TestMethod]
        public void InsertTest()
        {
            var task1 = QuestionManager.Load();
            task1.Wait();
            Guid questionId = task1.Result.FirstOrDefault().Id;

            var task2 = AnswerManager.Load(questionId);
            task2.Wait();
            Guid answerId = task2.Result.FirstOrDefault().Id;

            Response response = new Response
            {
                QuestionId = questionId,
                AnswerId = answerId
            };

            var task3 = ResponseManager.Insert(response, true);
            task3.Wait();
            Assert.IsTrue(task3.Result > 0);
        }

        [TestMethod]
        public void LoadByQuestionIdTest()
        {
            var task1 = QuestionManager.LoadByActivationCode("test01", new DateTime(2021, 2, 15));
            task1.Wait();
            Guid questionId = task1.Result.Id;

            var task2 = ResponseManager.LoadByQuestionId(questionId);
            task2.Wait();
            Assert.AreEqual(2, task2.Result.Count);
        }
    }
}
