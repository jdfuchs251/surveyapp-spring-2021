﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using JDF.SurveyMaker.BL;
using JDF.SurveyMaker.BL.Models;

namespace JDF.SurveyMaker.BL.Test
{
    [TestClass]
    public class utQuestionAnswer
    {
        [TestMethod]
        public void DeleteTest()
        {
            var task1 = QuestionManager.Load();
            task1.Wait();
            List<Question> questions = task1.Result;

            Question question = questions.First();

            var task2 = QuestionAnswerManager.Delete(question, true);
            task2.Wait();
            Assert.IsTrue(task2.Result);
        }

        [TestMethod]
        public void InsertTest()
        {
            var task1 = QuestionManager.Load();
            task1.Wait();
            List<Question> questions = task1.Result;

            Question question = questions.First(); // This question has 3 answers total

            question.Answers[0].IsCorrect = !question.Answers[0].IsCorrect;
            question.Answers[1].IsCorrect = !question.Answers[1].IsCorrect;

            var task2 = QuestionAnswerManager.Insert(question, true);
            task2.Wait();
            Assert.AreEqual(3, task2.Result);
        }
    }
}
