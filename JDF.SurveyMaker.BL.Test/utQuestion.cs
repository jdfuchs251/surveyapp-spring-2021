﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using JDF.SurveyMaker.BL;
using JDF.SurveyMaker.BL.Models;

namespace JDF.SurveyMaker.BL.Test
{
    [TestClass]
    public class utQuestion
    {
        [TestMethod]
        public void LoadTest()
        {
            var task = QuestionManager.Load();
            task.Wait();
            List<Question> questions = task.Result;
            Assert.AreEqual(5, questions.Count);
        }

        [TestMethod]
        public void LoadByIdTest()
        {
            // Get all questions
            var task1 = QuestionManager.Load();
            task1.Wait();
            List<Question> questions = task1.Result;

            Guid questionId = questions.First().Id;

            // Get the question by the Id just retrieved
            var task2 = QuestionManager.LoadById(questionId);
            task2.Wait();
            Question question = task2.Result;
            Assert.IsNotNull(question);
        }

        [TestMethod]
        public void LoadQuestionActivationTest()
        {
            var task = QuestionManager.Load();
            task.Wait();
            List<Question> questions = task.Result;

            int activationCount = 0;
            foreach (Question question in questions) // Count the total number of activations in the database
            {
                activationCount += question.Activations.Count; // Should be only 1 activation per question
            }

            Assert.AreEqual(3, activationCount);
        }

        [TestMethod]
        public void LoadByActivationCodeTest()
        {
            var task = QuestionManager.LoadByActivationCode("test02", DateTime.Now); // Test should fail if activation is not active for this date
            task.Wait();
            Question question = task.Result;

            Assert.IsNotNull(question);
        }

        [TestMethod]
        public void InsertTest()
        {
            Question question = new Question { Text = "Test question" };
            var task = QuestionManager.Insert(question, true);
            task.Wait();
            int results = task.Result;
            Assert.AreEqual(1, results);
        }

        [TestMethod]
        public void UpdateTest()
        {
            // Get all questions
            var task1 = QuestionManager.Load();
            task1.Wait();
            List<Question> questions = task1.Result;

            Question question = questions.First();
            question.Text = "Updated question.";

            var task2 = QuestionManager.Update(question, true);
            task2.Wait();
            int results = task2.Result;
            Assert.AreEqual(1, results);
        }

        [TestMethod]
        public void DeleteTest()
        {
            Question testQuestion = new Question { Text = "Test question" };
            var task1 = QuestionManager.Insert(testQuestion); // Don't roll back for delete test
            task1.Wait();
            int inserted = task1.Result;

            if (inserted == 1)
            {
                var task2 = QuestionManager.Delete(testQuestion); // Delete without rolling back
                task2.Wait();
                int results = task2.Result;
                Assert.AreEqual(1, results);
            }
        }
    }
}
