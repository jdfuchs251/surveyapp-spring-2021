using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using JDF.SurveyMaker.BL;
using JDF.SurveyMaker.BL.Models;
using JDF.SurveyMaker.PL;

namespace JDF.SurveyMaker.BL.Test
{
    [TestClass]
    public class utAnswer
    {
        [TestMethod]
        public void LoadTest()
        {
            /*Task.Run(async () =>
            {
                var task = await AnswerManager.Load();
                List<Answer> answers = task;
                Assert.AreEqual(2, answers.Count);
            }); */

            // Need to run tests synchronously because the above method always returns true

            var task = AnswerManager.Load();
            task.Wait();
            List<Answer> answers = task.Result;
            Assert.AreEqual(5, answers.Count);
        }

        [TestMethod]
        public void LoadByQuestionTest()
        {
            var task1 = QuestionManager.Load();
            task1.Wait();
            List<Question> questions = task1.Result;

            Guid questionId = questions.First().Id;

            var task2 = AnswerManager.Load(questionId);
            task2.Wait();
            List<Answer> answers = task2.Result;

            Assert.IsTrue(answers.Count > 0);
        }

        [TestMethod]
        public void LoadByIdTest()
        {
            // Get all answers
            var task1 = AnswerManager.Load();
            task1.Wait();
            List<Answer> answers = task1.Result;

            Guid answerId = answers.First().Id;

            // Get the answer by the Id just retrieved
            var task2 = AnswerManager.LoadById(answerId);
            task2.Wait();
            Answer answer = task2.Result;
            Assert.IsNotNull(answer);
        }

        [TestMethod]
        public void InsertTest()
        {
            Answer answer = new Answer { Text = "Test answer" };
            var task = AnswerManager.Insert(answer, true);
            task.Wait();
            bool success = task.Result;
            Assert.IsTrue(success);
        }

        [TestMethod]
        public void UpdateTest()
        {
            // Get all answers
            var task1 = AnswerManager.Load();
            task1.Wait();
            List<Answer> answers = task1.Result;

            Answer answer = answers.First();
            answer.Text = "Updated answer.";

            var task2 = AnswerManager.Update(answer, true);
            task2.Wait();
            int results = task2.Result;
            Assert.AreEqual(1, results);
        }

        [TestMethod]
        public void DeleteTest()
        {
            Answer testAnswer = new Answer { Text = "Test answer" };
            var task1 = AnswerManager.Insert(testAnswer); // Don't roll back for delete test
            task1.Wait();
            bool success = task1.Result;

            if (success)
            {
                var task2 = AnswerManager.Delete(testAnswer); // Delete without rolling back
                task2.Wait();
                int results = task2.Result;
                Assert.AreEqual(1, results); 
            }
        }
    }
}
