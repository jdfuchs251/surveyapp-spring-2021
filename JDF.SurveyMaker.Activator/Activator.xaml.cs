﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using JDF.SurveyMaker.BL.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Linq;

namespace JDF.SurveyMaker.Activator
{
    /// <summary>
    /// Interaction logic for Activator.xaml
    /// </summary>
    public partial class Activator : Window
    {
        List<Question> questions;
        Question question;
        List<Activation> activations;
        Activation activation;

        public Activator()
        {
            InitializeComponent();

            LoadQuestions(-1);
        }

        private void LoadQuestions(int selectIndex)
        {
            try
            {
                // Get the questions using the API
                HttpClient client = InitializeClient();
                var response = client.GetAsync("Question").Result;
                var result = response.Content.ReadAsStringAsync().Result;
                var items = (JArray)JsonConvert.DeserializeObject(result);

                // Populate local list of questions
                questions = items.ToObject<List<Question>>();

                // Bind questions to the combo box
                cboQuestions.ItemsSource = null;
                cboQuestions.ItemsSource = questions;
                cboQuestions.DisplayMemberPath = "Text";
                cboQuestions.SelectedValuePath = "Id";

                if (selectIndex > -1) cboQuestions.SelectedIndex = selectIndex;
            }
            catch (Exception)
            {
                MessageBox.Show("Error loading survey questions.", "Error");
            }
        }

        private void cboQuestions_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cboQuestions.SelectedIndex > -1)
            {
                // Get the selected question
                question = questions[cboQuestions.SelectedIndex];

                // Bind activations to the data grid view
                activations = question.Activations;
                BindActivations(-1);

                // Enable activation fields
                dteStartDate.IsEnabled = true;
                dteEndDate.IsEnabled = true;
                txtActivationCode.IsEnabled = true;
                btnInsert.IsEnabled = true;
            }
        }

        private void BindActivations()
        {
            // Bind activations
            dgvActivations.ItemsSource = null;
            dgvActivations.ItemsSource = activations;

            // Set up columns
            dgvActivations.Columns[0].Visibility = Visibility.Hidden; // Hide Activation Id
            dgvActivations.Columns[1].Visibility = Visibility.Hidden; // Hide Question Id
            dgvActivations.Columns[2].Visibility = Visibility.Hidden; // Hide Start Date DateTime
            dgvActivations.Columns[4].Visibility = Visibility.Hidden; // Hide End Date DateTime

            dgvActivations.Columns[3].Header = "Start Date";
            dgvActivations.Columns[5].Header = "End Date";
            dgvActivations.Columns[6].Header = "Activation Code";

            dgvActivations.Columns[3].Width = 100;
            dgvActivations.Columns[5].Width = 100;
            dgvActivations.Columns[6].Width = 100;

            dgvActivations.Columns[3].CanUserSort = false;
            dgvActivations.Columns[5].CanUserSort = false;
            dgvActivations.Columns[6].CanUserSort = false;
        }

        private void BindActivations(int selectIndex)
        {
            BindActivations();

            // Select row
            if (selectIndex > -1) dgvActivations.SelectedIndex = selectIndex;
        }

        private void BindActivations(Activation selectActivation)
        {
            BindActivations();

            // Select activation
            dgvActivations.SelectedItem = selectActivation;
        }

        private void dgvActivations_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgvActivations.SelectedIndex > -1) // An activation is selected
            {
                // Set form fields
                activation = (Activation)dgvActivations.SelectedItem;
                dteStartDate.SelectedDate = activation.StartDate;
                dteEndDate.SelectedDate = activation.EndDate;
                txtActivationCode.Text = activation.ActivationCode;

                // Enable update and delete
                btnUpdate.IsEnabled = true;
                btnDelete.IsEnabled = true;
            }
            else // No activation selected
            {
                // Clear form fields
                dteStartDate.SelectedDate = null;
                dteEndDate.SelectedDate = null;
                txtActivationCode.Text = string.Empty;

                // Disable update and delete
                btnUpdate.IsEnabled = false;
                btnDelete.IsEnabled = false;
            }
        }

        private void btnInsert_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!ValidateActivation()) return; // Make sure form data is valid

                // Create the new activation using the data on the form
                Activation newActivation = new Activation
                {
                    QuestionId = question.Id,
                    StartDate = (DateTime)dteStartDate.SelectedDate,
                    EndDate = (DateTime)dteEndDate.SelectedDate,
                    ActivationCode = txtActivationCode.Text
                };

                // Call the API
                HttpClient client = InitializeClient();
                string serializedObject = JsonConvert.SerializeObject(newActivation);
                var content = new StringContent(serializedObject);
                content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                var response = client.PostAsync("Activation", content).Result;

                if (response.IsSuccessStatusCode) // If the insert succeeded
                {
                    LoadQuestions(cboQuestions.SelectedIndex); // Reload to get the new Guid for the inserted activation
                    BindActivations(activations.FirstOrDefault(a => a.StartDate == newActivation.StartDate)); // This should grab the right one since dates cannot overlap
                }
                else throw new Exception(); // If the insert failed
            }
            catch (Exception)
            {
                MessageBox.Show("Error adding new activation to the database. Make sure dates do not overlap an existing activation.", "Error");
            }
        }

        private bool ValidateActivation()
        {
            if (dteStartDate.SelectedDate == null)
            {
                MessageBox.Show("Start date is empty.", "Error");
                return false;
            }
            if (dteEndDate.SelectedDate == null)
            {
                MessageBox.Show("End date is empty.", "Error");
                return false;
            }
            if (txtActivationCode.Text == string.Empty)
            {
                MessageBox.Show("Activation code is empty.", "Error");
                return false;
            }
            if (((DateTime)dteStartDate.SelectedDate).CompareTo((DateTime)dteEndDate.SelectedDate) > 0)
            {
                MessageBox.Show("Start date may not be later than end date.", "Error");
                return false;
            }

            return true;
        }

        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!ValidateActivation()) return; // Make sure form data is valid
                
                // Temporary activation for update - don't update the local list until we know the DB update worked to keep in sync
                Activation updatedActivation = new Activation
                {
                    Id = activation.Id,
                    QuestionId = question.Id,
                    StartDate = (DateTime)dteStartDate.SelectedDate,
                    EndDate = (DateTime)dteEndDate.SelectedDate,
                    ActivationCode = txtActivationCode.Text
                };

                // Call the API
                HttpClient client = InitializeClient();
                string serializedObject = JsonConvert.SerializeObject(updatedActivation);
                var content = new StringContent(serializedObject);
                content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                var response = client.PutAsync("Activation/" + updatedActivation.Id, content).Result;

                if (response.IsSuccessStatusCode) // If the update succeeded
                {
                    question.Activations[dgvActivations.SelectedIndex] = updatedActivation; // Sync local list with the activation updated in the DB
                    BindActivations(activations.FirstOrDefault(a => a.StartDate == updatedActivation.StartDate));
                }
                else throw new Exception(); // If the update failed
            }
            catch (Exception)
            {
                MessageBox.Show("Error updating activation in the database. Make sure dates do not overlap an existing activation.", "Error");
            }
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Call the API
                HttpClient client = InitializeClient();
                var response = client.DeleteAsync("Activation/" + activation.Id).Result;

                if (response.IsSuccessStatusCode) // If the delete succeeded
                {
                    int selectedIndex = dgvActivations.SelectedIndex;
                    question.Activations.Remove(activation);
                    if (selectedIndex == question.Activations.Count) selectedIndex--; // Decrement selected index to keep in range
                    BindActivations(selectedIndex);
                }
                else throw new Exception(); // If the delete failed
            }
            catch (Exception)
            {
                MessageBox.Show("Error deleting activation from the database.", "Error");
            }
        }

        private static HttpClient InitializeClient()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("https://localhost:44332/api/");
            return client;
        }
    }
}
