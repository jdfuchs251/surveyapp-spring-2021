﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JDF.SurveyMaker.BL.Models
{
    public class Activation
    {
        public Guid Id { get; set; }
        public Guid QuestionId { get; set; }

        public DateTime StartDate { get; set; }
        public string StartDateDisplay
        {
            get
            {
                return StartDate.ToString("d");
            }
        }

        public DateTime EndDate { get; set; }
        public string EndDateDisplay
        {
            get
            {
                return EndDate.ToString("d");
            }
        }

        public string ActivationCode { get; set; }
    }
}
