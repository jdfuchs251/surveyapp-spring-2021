﻿using System;

namespace JDF.SurveyMaker.BL.Models
{
    public class Answer
    {
        public Guid Id { get; set; }
        public bool IsCorrect { get; set; }
        public string Text { get; set; }
    }
}
