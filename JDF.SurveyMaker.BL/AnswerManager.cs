﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JDF.SurveyMaker.PL;
using JDF.SurveyMaker.BL.Models;
using Microsoft.EntityFrameworkCore.Storage;

namespace JDF.SurveyMaker.BL
{
    public class AnswerManager
    {
        public async static Task<List<Answer>> Load()
        {
            try
            {
                List<Answer> answers = new List<Answer>();

                using (SurveyEntities dc = new SurveyEntities())
                {
                    dc.tblAnswers.ToList().ForEach(a => answers.Add(new Answer
                    {
                        Id = a.Id,
                        Text = a.Answer
                    }));
                    return answers;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async static Task<List<Answer>> Load(Guid questionId)
        {
            try
            {
                List<Answer> answers = new List<Answer>();

                using (SurveyEntities dc = new SurveyEntities())
                {
                    /* I used this code before knowing about Entity Framework lazy loading proxies
                    // Get answers associated with the question
                    var answerRows = (from a in dc.tblAnswers
                                      join qa in dc.tblQuestionAnswers
                                      on a.Id equals qa.AnswerId
                                      where qa.QuestionId == questionId
                                      select new
                                      {
                                          Id = a.Id,
                                          Text = a.Answer,
                                          IsCorrect = qa.IsCorrect
                                      }).ToList();

                    answerRows.ForEach(a => answers.Add(new Answer
                    {
                        Id = a.Id,
                        Text = a.Text,
                        IsCorrect = a.IsCorrect
                    }));
                    */

                    dc.tblQuestionAnswers.Where(qa => qa.QuestionId == questionId)
                        .ToList().ForEach(qa => answers.Add(new Answer 
                    { 
                        Id = qa.AnswerId,
                        Text = qa.Answer.Answer,
                        IsCorrect = qa.IsCorrect
                    }));

                    return answers;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async static Task<bool> Insert(Answer answer, bool rollback = false)
        {
            try
            {
                IDbContextTransaction transaction = null;

                using (SurveyEntities dc = new SurveyEntities())
                {
                    if (rollback) transaction = dc.Database.BeginTransaction();

                    tblAnswer newRow = new tblAnswer
                    {
                        Id = Guid.NewGuid(),
                        Answer = answer.Text
                    };

                    dc.tblAnswers.Add(newRow);
                    bool success = dc.SaveChanges() > 0;

                    if (success) answer.Id = newRow.Id; // Backfill Id if the insert worked

                    if (rollback) transaction.Rollback();

                    return success;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async static Task<int> Update(Answer answer, bool rollback = false)
        {
            try
            {
                IDbContextTransaction transaction = null;

                using (SurveyEntities dc = new SurveyEntities())
                {
                    tblAnswer row = dc.tblAnswers.FirstOrDefault(a => a.Id == answer.Id);

                    if (row != null)
                    {
                        int results = 0;
                        if (rollback) transaction = dc.Database.BeginTransaction();

                        row.Answer = answer.Text;

                        results = dc.SaveChanges();

                        if (rollback) transaction.Rollback();

                        return results;
                    }
                    else
                    {
                        throw new Exception("Row not found.");
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async static Task<int> Delete(Answer answer, bool rollback = false)
        {
            try
            {
                IDbContextTransaction transaction = null;

                using (SurveyEntities dc = new SurveyEntities())
                {
                    tblAnswer row = dc.tblAnswers.FirstOrDefault(a => a.Id == answer.Id);

                    if (row != null)
                    {
                        int results = 0;
                        if (rollback) transaction = dc.Database.BeginTransaction();

                        dc.tblAnswers.Remove(row);

                        results = dc.SaveChanges();

                        if (rollback) transaction.Rollback();

                        return results;
                    }
                    else
                    {
                        throw new Exception("Row not found.");
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async static Task<Answer> LoadById(Guid answerId)
        {
            try
            {
                using (SurveyEntities dc = new SurveyEntities())
                {
                    var row = dc.tblAnswers.FirstOrDefault(a => a.Id == answerId);
                    
                    if (row != null)
                    {
                        Answer answer = new Answer
                        {
                            Id = row.Id,
                            Text = row.Answer
                        };
                        return answer;
                    }
                    else
                    {
                        throw new Exception("Row not found.");
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
