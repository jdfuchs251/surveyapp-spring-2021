﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JDF.SurveyMaker.PL;
using JDF.SurveyMaker.BL.Models;
using Microsoft.EntityFrameworkCore.Storage;

namespace JDF.SurveyMaker.BL
{
    public class QuestionAnswerManager
    {
        public async static Task<int> Insert(Question question, bool rollback = false)
        {
            try
            {
                // Delete any existing question/answer associations before doing the insert
                var task = Delete(question, rollback);
                task.Wait();
                bool deleteSucceeded = task.Result;

                if (deleteSucceeded) // Proceed with insertion
                {
                    IDbContextTransaction transaction = null;

                    using (SurveyEntities dc = new SurveyEntities())
                    {
                        int results = 0;
                        if (rollback) transaction = dc.Database.BeginTransaction();

                        foreach (Answer answer in question.Answers)
                        {
                            dc.tblQuestionAnswers.Add(new tblQuestionAnswer
                            {
                                Id = Guid.NewGuid(),
                                QuestionId = question.Id,
                                AnswerId = answer.Id,
                                IsCorrect = answer.IsCorrect
                            });
                        }

                        results = dc.SaveChanges();

                        if (rollback) transaction.Rollback();

                        return results;
                    }
                }
                else
                {
                    throw new Exception("Could not update question answers.");
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async static Task<bool> Delete(Question question, bool rollback = false)
        {
            try
            {
                IDbContextTransaction transaction = null;

                using (SurveyEntities dc = new SurveyEntities())
                {
                    var rows = dc.tblQuestionAnswers.Where(qa => qa.QuestionId == question.Id);

                    if (rows.Count() > 0) // Found existing rows associated with this question
                    {
                        bool success;
                        if (rollback) transaction = dc.Database.BeginTransaction();

                        dc.tblQuestionAnswers.RemoveRange(rows);

                        success = dc.SaveChanges() > 0;

                        if (rollback) transaction.Rollback();

                        return success;
                    }
                    else
                    {
                        return true; // No existing rows to delete
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
