﻿using JDF.SurveyMaker.BL.Models;
using JDF.SurveyMaker.PL;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JDF.SurveyMaker.BL
{
    public class ActivationManager
    {
        public async static Task<int> Insert(Activation activation, bool rollback = false)
        {
            try
            {
                IDbContextTransaction transaction = null;

                using (SurveyEntities dc = new SurveyEntities())
                {
                    // Check date range - only 1 activation is allowed for any given date
                    dc.tblActivations.ToList().ForEach(a =>
                    {
                        if (ActivationDatesOverlap(a, activation))
                            throw new Exception("Activation date range overlaps an existing activation.");
                    });

                    int results = 0;
                    if (rollback) transaction = dc.Database.BeginTransaction();

                    tblActivation newRow = new tblActivation
                    {
                        Id = Guid.NewGuid(),
                        QuestionId = activation.QuestionId,
                        StartDate = GetAdjustedDate(activation.StartDate), // Strip any time units - DateTime values will always be stored with a time of midnight
                        EndDate = GetAdjustedDate(activation.EndDate),
                        ActivationCode = activation.ActivationCode
                    };

                    dc.tblActivations.Add(newRow);
                    results = dc.SaveChanges();

                    if (results > 0) activation.Id = newRow.Id; // Backfill Id if the insert worked

                    if (rollback) transaction.Rollback();

                    return results;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async static Task<int> Update(Activation activation, bool rollback = false)
        {
            try
            {
                IDbContextTransaction transaction = null;

                using (SurveyEntities dc = new SurveyEntities())
                {
                    // Check date range
                    dc.tblActivations.ToList().ForEach(a =>
                    {
                        if (a.Id != activation.Id && ActivationDatesOverlap(a, activation))
                            throw new Exception("Activation date range overlaps an existing activation.");
                    });

                    tblActivation row = dc.tblActivations.FirstOrDefault(a => a.Id == activation.Id);

                    if (row != null)
                    {
                        int results = 0;
                        if (rollback) transaction = dc.Database.BeginTransaction();

                        row.QuestionId = activation.QuestionId;
                        row.StartDate = GetAdjustedDate(activation.StartDate); // Strip any time units - DateTime values will always be stored with a time of midnight
                        row.EndDate = GetAdjustedDate(activation.EndDate);
                        row.ActivationCode = activation.ActivationCode;

                        results = dc.SaveChanges();

                        if (rollback) transaction.Rollback();

                        return results;
                    }
                    else
                    {
                        throw new Exception("Row not found.");
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async static Task<int> Delete(Guid id, bool rollback = false)
        {
            try
            {
                IDbContextTransaction transaction = null;

                using (SurveyEntities dc = new SurveyEntities())
                {
                    tblActivation row = dc.tblActivations.FirstOrDefault(a => a.Id == id);

                    if (row != null)
                    {
                        int results = 0;
                        if (rollback) transaction = dc.Database.BeginTransaction();

                        dc.tblActivations.Remove(row);

                        results = dc.SaveChanges();

                        if (rollback) transaction.Rollback();

                        return results;
                    }
                    else
                    {
                        throw new Exception("Row not found.");
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool ActivationDatesOverlap(tblActivation baseActivation, Activation testActivation) // Used to determine whether activation dates overlap
        {
            // For the test activation to have valid dates, both its start and end dates must fall outside the span of the base start and end dates

            DateTime testStart = new DateTime(testActivation.StartDate.Year, testActivation.StartDate.Month, testActivation.StartDate.Day);
            if (testStart.CompareTo(baseActivation.StartDate) >= 0 && testStart.CompareTo(baseActivation.EndDate) <= 0) return true;

            DateTime testEnd = new DateTime(testActivation.EndDate.Year, testActivation.EndDate.Month, testActivation.EndDate.Day);
            if (testEnd.CompareTo(baseActivation.StartDate) >= 0 && testEnd.CompareTo(baseActivation.EndDate) <= 0) return true;

            return false;
        }

        private static DateTime GetAdjustedDate(DateTime date)
        {
            DateTime adjustedDate = new DateTime(date.Year, date.Month, date.Day); // Strip off time units from date value
            return adjustedDate;
        }
    }
}
