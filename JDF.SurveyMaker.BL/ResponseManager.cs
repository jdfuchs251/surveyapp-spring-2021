﻿using JDF.SurveyMaker.BL.Models;
using JDF.SurveyMaker.PL;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JDF.SurveyMaker.BL
{
    public class ResponseManager
    {
        public async static Task<int> Insert(Response response, bool rollback = false)
        {
            try
            {
                IDbContextTransaction transaction = null;

                using (SurveyEntities dc = new SurveyEntities())
                {
                    int results = 0;
                    if (rollback) transaction = dc.Database.BeginTransaction();

                    tblResponse newRow = new tblResponse
                    {
                        Id = Guid.NewGuid(),
                        QuestionId = response.QuestionId,
                        AnswerId = response.AnswerId,
                        ResponseDate = DateTime.Now // Use current time
                    };

                    dc.tblResponses.Add(newRow);
                    results = dc.SaveChanges();

                    if (results > 0) response.Id = newRow.Id; // Backfill Id if the insert worked

                    if (rollback) transaction.Rollback();

                    return results;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async static Task<List<Response>> LoadByQuestionId(Guid questionId)
        {
            try
            {
                using (SurveyEntities dc = new SurveyEntities())
                {
                    List<Response> responses = new List<Response>();
                    var rows = dc.tblResponses.Where(r => r.QuestionId == questionId);
                    if (rows.Any())
                    {
                        rows.ToList().ForEach(r => responses.Add(new Response
                        {
                            Id = r.Id,
                            QuestionId = r.QuestionId,
                            AnswerId = r.AnswerId,
                            ResponseDate = r.ResponseDate
                        }));
                    }
                    return responses;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
