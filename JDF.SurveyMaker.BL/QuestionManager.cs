﻿using System;
using JDF.SurveyMaker.PL;
using JDF.SurveyMaker.BL.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore.Storage;

namespace JDF.SurveyMaker.BL
{
    public class QuestionManager
    {
        public async static Task<List<Question>> Load()
        {
            try
            {
                List<Question> questions = new List<Question>();

                using (SurveyEntities dc = new SurveyEntities())
                {
                    dc.tblQuestions.ToList().ForEach(q =>
                    {
                        // Load answers
                        List<Answer> answers = new List<Answer>();
                        var task = AnswerManager.Load(q.Id);
                        task.Wait();
                        answers = task.Result;

                        // Load activations
                        List<Activation> activations = new List<Activation>();
                        q.tblActivations.ToList().ForEach(a => activations.Add(new Activation
                        {
                            Id = a.Id,
                            QuestionId = a.QuestionId,
                            StartDate = a.StartDate,
                            EndDate = a.EndDate,
                            ActivationCode = a.ActivationCode
                        }));

                        // Add question
                        questions.Add(new Question
                        {
                            Id = q.Id,
                            Text = q.Question,
                            Answers = answers,
                            Activations = activations
                        });
                    });

                    return questions;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async static Task<int> Insert(Question question, bool rollback = false)
        {
            try
            {
                IDbContextTransaction transaction = null;

                using (SurveyEntities dc = new SurveyEntities())
                {
                    int results = 0;
                    if (rollback) transaction = dc.Database.BeginTransaction();

                    tblQuestion newRow = new tblQuestion
                    {
                        Id = Guid.NewGuid(),
                        Question = question.Text
                    };

                    dc.tblQuestions.Add(newRow);
                    results = dc.SaveChanges();

                    if (results > 0) question.Id = newRow.Id; // Backfill Id if the insert worked

                    if (rollback) transaction.Rollback();

                    return results;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async static Task<int> Update(Question question, bool rollback = false)
        {
            try
            {
                IDbContextTransaction transaction = null;

                using (SurveyEntities dc = new SurveyEntities())
                {
                    tblQuestion row = dc.tblQuestions.FirstOrDefault(q => q.Id == question.Id);

                    if (row != null)
                    {
                        int results = 0;
                        if (rollback) transaction = dc.Database.BeginTransaction();

                        row.Question = question.Text;

                        results = dc.SaveChanges();

                        if (rollback) transaction.Rollback();

                        return results;
                    }
                    else
                    {
                        throw new Exception("Row not found.");
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async static Task<int> Delete(Question question, bool rollback = false)
        {
            try
            {
                IDbContextTransaction transaction = null;

                using (SurveyEntities dc = new SurveyEntities())
                {
                    tblQuestion row = dc.tblQuestions.FirstOrDefault(q => q.Id == question.Id);

                    if (row != null)
                    {
                        int results = 0;
                        if (rollback) transaction = dc.Database.BeginTransaction();

                        dc.tblQuestions.Remove(row);

                        results = dc.SaveChanges();

                        if (rollback) transaction.Rollback();

                        return results;
                    }
                    else
                    {
                        throw new Exception("Row not found.");
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async static Task<Question> LoadById(Guid questionId)
        {
            try
            {
                using (SurveyEntities dc = new SurveyEntities())
                {
                    var row = dc.tblQuestions.FirstOrDefault(q => q.Id == questionId);

                    if (row != null)
                    {
                        // Load answers
                        List<Answer> answers = new List<Answer>();
                        var task = AnswerManager.Load(row.Id);
                        task.Wait();
                        answers = task.Result;

                        // Load activations
                        List<Activation> activations = new List<Activation>();
                        row.tblActivations.ToList().ForEach(a => activations.Add(new Activation
                        {
                            Id = a.Id,
                            QuestionId = a.QuestionId,
                            StartDate = a.StartDate,
                            EndDate = a.EndDate,
                            ActivationCode = a.ActivationCode
                        }));

                        Question question = new Question
                        {
                            Id = row.Id,
                            Text = row.Question,
                            Answers = answers,
                            Activations = activations
                        };

                        return question;
                    }
                    else
                    {
                        throw new Exception("Row not found.");
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async static Task<Question> LoadByActivationCode(string activationCode, DateTime date) // Only get the question if the activation is active for this date
        {
            try
            {
                date = new DateTime(date.Year, date.Month, date.Day); // Strip time units so search works if date is the same day as activation end date

                using (SurveyEntities dc = new SurveyEntities())
                {
                    var activationRow = dc.tblActivations.FirstOrDefault(a => a.ActivationCode == activationCode && // Activation code matches
                                                                              date.CompareTo(a.StartDate) >= 0 &&  // Date is after or equal to activation start date
                                                                              date.CompareTo(a.EndDate) <= 0); // Date is before or equal to activation end date

                    if (activationRow != null)
                    {
                        // Load answers
                        var task = AnswerManager.Load(activationRow.QuestionId); // Load from DB to get whether question answer is correct
                        task.Wait();
                        List<Answer> answers = task.Result;

                        // Set activation list
                        List<Activation> activations = new List<Activation>();
                        Activation activation = new Activation
                        {
                            Id = activationRow.Id,
                            QuestionId = activationRow.QuestionId,
                            StartDate = activationRow.StartDate,
                            EndDate = activationRow.EndDate,
                            ActivationCode = activationRow.ActivationCode
                        };
                        activations.Add(activation);

                        Question question = new Question
                        {
                            Id = activationRow.QuestionId,
                            Text = activationRow.Question.Question,
                            Answers = answers,
                            Activations = activations
                        };

                        return question;
                    }
                    else
                    {
                        throw new Exception("Activation code not found.");
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
