﻿using JDF.SurveyMaker.BL;
using JDF.SurveyMaker.BL.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace JDF.SurveyMaker.API.Test
{
    [TestClass]
    public class utResponse
    {
        private static HttpClient InitializeClient()
        {
            var server = new TestServer(new WebHostBuilder().UseEnvironment("Development").UseStartup<Startup>());
            var client = server.CreateClient();
            client.BaseAddress = new Uri("https://localhost:44332/api/");
            return client;
        }

        [TestMethod]
        public void InsertTest()
        {
            // Make a new response using existing question and answer Ids
            var task1 = QuestionManager.Load();
            task1.Wait();
            Guid questionId = task1.Result.FirstOrDefault().Id;

            var task2 = AnswerManager.Load(questionId);
            task2.Wait();
            Guid answerId = task2.Result.FirstOrDefault().Id;

            Response response = new Response
            {
                QuestionId = questionId,
                AnswerId = answerId
            };

            // Insert the response
            HttpClient client = InitializeClient();

            string serializedObject = JsonConvert.SerializeObject(response);
            var content = new StringContent(serializedObject);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
            var postResponse = client.PostAsync("Response", content).Result;

            Assert.IsTrue(postResponse.IsSuccessStatusCode);
        }
    }
}
