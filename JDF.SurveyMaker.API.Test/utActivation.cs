﻿using JDF.SurveyMaker.BL.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace JDF.SurveyMaker.API.Test
{
    [TestClass]
    public class utActivation
    {
        private static HttpClient InitializeClient()
        {
            var server = new TestServer(new WebHostBuilder().UseEnvironment("Development").UseStartup<Startup>());
            var client = server.CreateClient();
            client.BaseAddress = new Uri("https://localhost:44332/api/");
            return client;
        }

        [TestMethod]
        public void InsertTest()
        {
            // Get a question ID to use for the activation
            HttpClient client = InitializeClient();
            var response = client.GetAsync("Question").Result;
            var result = response.Content.ReadAsStringAsync().Result;
            var items = (JArray)JsonConvert.DeserializeObject(result);

            Guid questionId = items.ToObject<List<Question>>().FirstOrDefault().Id;

            // Create the activation
            Activation activation = new Activation
            {
                QuestionId = questionId,
                StartDate = new DateTime(2021, 4, 1), // Test will fail if dates overlap an existing acivation in the DB
                EndDate = new DateTime(2021, 4, 2),
                ActivationCode = "test04"
            };

            string serializedObject = JsonConvert.SerializeObject(activation);
            var content = new StringContent(serializedObject);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
            response = client.PostAsync("Activation", content).Result;

            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [TestMethod]
        public void UpdateTest()
        {
            HttpClient client = InitializeClient();

            var response = client.GetAsync("Question/test03").Result; // Must use an active activation code or this won't find a question
            var result = response.Content.ReadAsStringAsync().Result;
            Question question = JsonConvert.DeserializeObject<Question>(result);

            Activation activation = question.Activations.LastOrDefault();
            activation.ActivationCode = "test05";

            string serializedObject = JsonConvert.SerializeObject(activation);
            var content = new StringContent(serializedObject);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
            response = client.PutAsync("Activation/" + activation.Id, content).Result;

            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [TestMethod]
        public void DeleteTest()
        {
            HttpClient client = InitializeClient();

            var response = client.GetAsync("Question/test05").Result; // Must use an active activation code or this won't find a question
            var result = response.Content.ReadAsStringAsync().Result;
            Question question = JsonConvert.DeserializeObject<Question>(result);

            Activation activation = question.Activations.LastOrDefault();

            response = client.DeleteAsync("Activation/" + activation.Id).Result;

            Assert.IsTrue(response.IsSuccessStatusCode);
        }
    }
}
