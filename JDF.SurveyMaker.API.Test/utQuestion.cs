﻿using JDF.SurveyMaker.BL.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace JDF.SurveyMaker.API.Test
{
    [TestClass]
    public class utQuestion
    {
        private static HttpClient InitializeClient()
        {
            var server = new TestServer(new WebHostBuilder().UseEnvironment("Development").UseStartup<Startup>());
            var client = server.CreateClient();
            client.BaseAddress = new Uri("https://localhost:44332/api/");
            return client;
        }

        [TestMethod]
        public void LoadTest()
        {
            HttpClient client = InitializeClient();
            var response = client.GetAsync("Question").Result;
            var result = response.Content.ReadAsStringAsync().Result;
            var items = (JArray)JsonConvert.DeserializeObject(result);

            List<Question> questions = items.ToObject<List<Question>>();

            Assert.IsTrue(questions.Count > 0);
        }

        [TestMethod]
        public void LoadByIdTest()
        {
            // Start by loading all to get the ID of a single question
            HttpClient client = InitializeClient();
            var response = client.GetAsync("Question").Result;
            var result = response.Content.ReadAsStringAsync().Result;
            var items = (JArray)JsonConvert.DeserializeObject(result);

            Guid questionId = items.ToObject<List<Question>>().FirstOrDefault().Id;

            response = client.GetAsync("Question/" + questionId).Result;
            result = response.Content.ReadAsStringAsync().Result;
            Question question = JsonConvert.DeserializeObject<Question>(result);

            Assert.IsNotNull(question);
        }

        [TestMethod]
        public void LoadByActivationCodeTest()
        {
            HttpClient client = InitializeClient();

            var response = client.GetAsync("Question/test03").Result;
            var result = response.Content.ReadAsStringAsync().Result;
            Question question = JsonConvert.DeserializeObject<Question>(result);

            Assert.IsNotNull(question);
        }

        /*
         * Inserts and updates are not needed for this project. The only records that will be inserted
         * or updated are activations.
         * 
         * [TestMethod]
        public void InsertTest()
        {
            HttpClient client = InitializeClient();
        }

        [TestMethod]
        public void UpdateTest()
        {
            HttpClient client = InitializeClient();
        }*/
    }
}
