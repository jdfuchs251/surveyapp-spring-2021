﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using JDF.SurveyMaker.BL;
using JDF.SurveyMaker.BL.Models;

namespace JDF.SurveyMaker.UI
{
    public enum ScreenMode
    {
        Question, Answer
    }

    /// <summary>
    /// Interaction logic for MaintainSurveyItem.xaml
    /// </summary>
    public partial class MaintainSurveyItem : Window
    {
        List<Question> questions;
        List<Answer> answers;
        ScreenMode screenMode;

        public MaintainSurveyItem(List<Question> questions) // Pass in reference to saved list of questions to minimize DB hits
        {
            InitializeComponent();
            screenMode = ScreenMode.Question;
            this.questions = questions;

            lblItemPrompt.Content = "Enter a Question";
            this.Title = "Maintain Survey Questions";

            cboItem.DisplayMemberPath = "Text";
            cboItem.SelectedValuePath = "Id";

            Rebind();
        }

        public MaintainSurveyItem(List<Answer> answers) // Pass in reference to saved list of answers to minimize DB hits
        {
            InitializeComponent();
            screenMode = ScreenMode.Answer;
            this.answers = answers;

            lblItemPrompt.Content = "Enter an Answer";
            this.Title = "Maintain Survey Answers";

            cboItem.DisplayMemberPath = "Text";
            cboItem.SelectedValuePath = "Id";

            Rebind();
        }

        /**********************************************************************************
        /* Name: Rebind()
        /* Description: Rebinds list of questions or answers to the combo box
        /*********************************************************************************/
        private void Rebind()
        {
            cboItem.ItemsSource = null;

            switch (screenMode)
            {
                case ScreenMode.Question:
                    questions.Sort((q1, q2) => q1.Text.CompareTo(q2.Text));
                    cboItem.ItemsSource = questions;
                    break;
                case ScreenMode.Answer:
                    answers.Sort((a1, a2) => a1.Text.CompareTo(a2.Text));
                    cboItem.ItemsSource = answers;
                    break;
            }
        }

        /**********************************************************************************
        /* Name: Rebind() - first overload
        /* Description: Rebinds list of questions to the combo box and selects a given question
        /* Parameters:
        /*   selectQuestion (Question) - Which question to select
        /*********************************************************************************/
        private void Rebind(Question selectQuestion)
        {
            Rebind();
            if (selectQuestion != null) cboItem.SelectedItem = selectQuestion;
        }

        /**********************************************************************************
        /* Name: Rebind() - second overload
        /* Description: Rebinds list of answers to the combo box and selects a given answer
        /* Parameters:
        /*   selectAnswer (Answer) - Which answer to select
        /*********************************************************************************/
        private void Rebind(Answer selectAnswer)
        {
            Rebind();
            if (selectAnswer != null) cboItem.SelectedItem = selectAnswer;
        }

        /**********************************************************************************
        /* Name: cboItem_SelectionChanged()
        /* Description: Sets text box and button properties depending on combo box selection
        /*********************************************************************************/
        private void cboItem_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cboItem.SelectedIndex > -1)
            {
                btnUpdate.IsEnabled = true;
                btnDelete.IsEnabled = true;

                switch (screenMode)
                {
                    case ScreenMode.Question:
                        txtItem.Text = questions[cboItem.SelectedIndex].Text;
                        break;
                    case ScreenMode.Answer:
                        txtItem.Text = answers[cboItem.SelectedIndex].Text;
                        break;
                }
            }
            else // No selection - disable update and delete (user must add an item or select from the combo box)
            {
                txtItem.Text = string.Empty;
                btnUpdate.IsEnabled = false;
                btnDelete.IsEnabled = false;
            }
        }

        /**********************************************************************************
        /* Name: btnAdd_Click()
        /* Description: Adds a question/answer to the database and to the local generic list
        /*********************************************************************************/
        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Task task;
                switch (screenMode)
                {
                    case ScreenMode.Question:
                        Question question = new Question { Text = txtItem.Text };
                        task = Task.Run(async () =>
                        {
                            await QuestionManager.Insert(question);
                        });
                        task.Wait(); // Wait for task to complete in order to catch any exceptions
                        questions.Add(question);
                        Rebind(question);
                        break;
                    case ScreenMode.Answer:
                        Answer answer = new Answer { Text = txtItem.Text };
                        task = Task.Run(async () =>
                        {
                            await AnswerManager.Insert(answer);
                        });
                        task.Wait(); // Wait for task to complete in order to catch any exceptions
                        answers.Add(answer);
                        Rebind(answer);
                        break;
                }
            }
            catch (Exception)
            {
                MessageBox.Show(screenMode.ToString() + " could not be added to the database.", "Error");
            }
        }

        /**********************************************************************************
        /* Name: btnUpdate_Click()
        /* Description: Updates a question/answer in the database and the local generic list
        /*********************************************************************************/
        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Task task;
                switch (screenMode)
                {
                    case ScreenMode.Question:
                        Question question = questions[cboItem.SelectedIndex];
                        question.Text = txtItem.Text;
                        task = Task.Run(async () =>
                        {
                            await QuestionManager.Update(question);
                        });
                        task.Wait(); // Wait for task to complete in order to catch any exceptions
                        Rebind(question);
                        break;
                    case ScreenMode.Answer:
                        Answer answer = answers[cboItem.SelectedIndex];
                        answer.Text = txtItem.Text;
                        task = Task.Run(async () =>
                        {
                            await AnswerManager.Update(answer);
                        });
                        task.Wait(); // Wait for task to complete in order to catch any exceptions
                        Rebind(answer);
                        break;
                }
            }
            catch (Exception)
            {
                MessageBox.Show(screenMode.ToString() + " could not be updated in the database.", "Error");
            }
        }

        /**********************************************************************************
        /* Name: btnDelete_Click()
        /* Description: Removes a question/answer from the database and the local generic list
        /*********************************************************************************/
        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Task task;
                switch (screenMode)
                {
                    case ScreenMode.Question:
                        Question question = questions[cboItem.SelectedIndex];
                        task = Task.Run(async () =>
                        {
                            await QuestionManager.Delete(question);
                        });
                        task.Wait(); // Wait for task to complete in order to catch any exceptions
                        questions.Remove(question);
                        break;
                    case ScreenMode.Answer:
                        Answer answer = answers[cboItem.SelectedIndex];
                        task = Task.Run(async () =>
                        {
                            await AnswerManager.Delete(answer);
                        });
                        task.Wait(); // Wait for task to complete in order to catch any exceptions
                        answers.Remove(answer);
                        break;
                }
                Rebind();
            }
            catch (Exception)
            {
                MessageBox.Show(screenMode.ToString() + " could not be deleted from the database. Remove from use in existing survey questions and try again.", "Error");
            }
        }
    }
}
