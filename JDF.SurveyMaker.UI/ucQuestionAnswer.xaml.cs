﻿using JDF.SurveyMaker.BL.Models;
using JDF.SurveyMaker.BL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace JDF.SurveyMaker.UI
{
    /// <summary>
    /// Interaction logic for ucQuestionAnswer.xaml
    /// </summary>
    public partial class ucQuestionAnswer : UserControl
    {
        List<Answer> answers;
        Answer answer;

        /**********************************************************************************
        /* Constructor
        /*********************************************************************************/

        public ucQuestionAnswer()
        {
            InitializeComponent();

            cboAnswers.DisplayMemberPath = "Text";
            cboAnswers.SelectedValuePath = "Id";

            this.imgDelete.MouseLeftButtonUp += ImgDelete_MouseLeftButtonUp;
        }

        /**********************************************************************************
        /* Begin public fields
        /*********************************************************************************/

        public Guid? AnswerId
        {
            get
            {
                return answer == null ? null : answer.Id;
            }
            set
            {
                if (value != null) // Update the combo box with the new value (this will handle the local answer variable)
                {
                    Answer newAnswer = answers.FirstOrDefault(a => a.Id == value);
                    if (newAnswer != null) cboAnswers.SelectedItem = newAnswer;
                }
            }
        }

        public Answer SelectedAnswer { get { return answer; } }

        public bool IsCorrect
        {
            get { return rdoIsCorrect.IsChecked == null ? false : (bool)rdoIsCorrect.IsChecked; }
            set { rdoIsCorrect.IsChecked = value; }
        }

        /**********************************************************************************
        /* Begin public methods
        /*********************************************************************************/

        /**********************************************************************************
        /* Name: ClearAnswer()
        /* Description: Clears the selection from a combo box
        /*********************************************************************************/
        public void ClearAnswer()
        {
            cboAnswers.SelectedIndex = -1;
        }

        /**********************************************************************************
        /* Name: BindAnswers()
        /* Description: Binds a list of answers to the combo box
        /* Parameters:
        /*   List<Answer> answerList - List of answers to be bound
        /*********************************************************************************/
        public void BindAnswers(List<Answer> answerList)
        {
            answers = answerList;
            cboAnswers.ItemsSource = null;

            answers.Sort((a1, a2) => a1.Text.CompareTo(a2.Text));
            cboAnswers.ItemsSource = answers;
        }

        /**********************************************************************************
        /* Name: BindAnswers() - first overload
        /* Description: Binds a list of answers to the combo box and selects a given answer
        /* Parameters:
        /*   List<Answer> answerList - List of answers to be bound
        /*      Guid? selectAnswerId - Id of answer to select in the combo box
        /*********************************************************************************/
        public void BindAnswers(List<Answer> answerList, Guid? selectAnswerId)
        {
            BindAnswers(answerList);
            if (selectAnswerId != null)
            {
                Answer selectedAnswer = answers.FirstOrDefault(a => a.Id == selectAnswerId);
                if (selectedAnswer != null) cboAnswers.SelectedItem = selectedAnswer;
            }
        }

        /**********************************************************************************
        /* Begin private methods
        /*********************************************************************************/

        /**********************************************************************************
        /* Name: ImgDelete_MouseLeftButtonUp()
        /* Description: Clears the selected answer when clicking the delete image
        /*********************************************************************************/
        private void ImgDelete_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ClearAnswer();
        }

        /**********************************************************************************
        /* Name: cboAnswers_SelectionChanged()
        /* Description: Handles controls when changing combo box selected value
        /*********************************************************************************/
        private void cboAnswers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cboAnswers.SelectedIndex > -1) // Selected an answer from the combo box
            {
                rdoIsCorrect.IsEnabled = true;
                answer = answers[cboAnswers.SelectedIndex];
            }
            else // No answer selected
            {
                rdoIsCorrect.IsChecked = false;
                rdoIsCorrect.IsEnabled = false;
                answer = null;
            }
        }
    }
}
