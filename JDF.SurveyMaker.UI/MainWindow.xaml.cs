﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using JDF.SurveyMaker.BL;
using JDF.SurveyMaker.BL.Models;

namespace JDF.SurveyMaker.UI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Question> questions;
        List<Answer> answers;
        Question question;
        const int NUM_ANSWER_FIELDS = 4; // Easily modify to include as many answer fields as you want
        ucQuestionAnswer[] AnswerFields = new ucQuestionAnswer[NUM_ANSWER_FIELDS];

        public MainWindow()
        {
            InitializeComponent();

            // Set up answer fields
            for (int i = 0; i < NUM_ANSWER_FIELDS; i++)
            {
                AnswerFields[i] = new ucQuestionAnswer();
                AnswerFields[i].Margin = new Thickness(100, 135 + 40 * i, 0, 0);
                grdSurveyMaker.Children.Add(AnswerFields[i]);
            }
            LoadAndBindAnswers();

            // Set up combo box
            cboQuestions.DisplayMemberPath = "Text";
            cboQuestions.SelectedValuePath = "Id";
            LoadAndBindQuestions();
        }

        /**********************************************************************************
        /* Name: LoadAndBindAnswers()
        /* Description: Loads answers from database and binds them to all custom control combo boxes
        /*********************************************************************************/
        private async void LoadAndBindAnswers()
        {
            try
            {
                foreach (ucQuestionAnswer field in AnswerFields) field.ClearAnswer(); // Clear fields so reload doesn't affect any selected answers

                answers = await AnswerManager.Load(); // Get updated answer list from the DB

                foreach (ucQuestionAnswer field in AnswerFields) field.BindAnswers(answers); // Rebind updated answer list

                if (question != null) LoadFieldAnswers(); // Reload answer fields from currently selected question
            }
            catch (Exception)
            {
                MessageBox.Show("Error loading answers from the database.", "Error");
            }
        }

        /**********************************************************************************
        /* Name: LoadFieldAnswers()
        /* Description: Loads the answers associated with a question into form custom controls
        /*********************************************************************************/
        private void LoadFieldAnswers()
        {
            for (int i = 0; i < question.Answers.Count; i++)
            {
                AnswerFields[i].AnswerId = question.Answers[i].Id;
                AnswerFields[i].IsCorrect = question.Answers[i].IsCorrect;
            }
        }

        /**********************************************************************************
        /* Name: LoadAndBindQuestions()
        /* Description: Loads questions from database and binds them to the combo box
        /*********************************************************************************/
        private async void LoadAndBindQuestions()
        {
            try
            {
                if (question != null) // Handle reload if a question is currently selected
                {
                    Guid questionId = question.Id;
                    questions.Remove(question);
                    questions = await QuestionManager.Load();
                    question = questions.FirstOrDefault(q => q.Id == questionId); // Reset question if still in the DB (should be there since FK constraint prevents deletion)
                }
                else
                {
                    questions = await QuestionManager.Load();
                }

                BindQuestions(question);
            }
            catch (Exception)
            {
                MessageBox.Show("Error loading questions from the database.", "Error");
            }
        }

        /**********************************************************************************
        /* Name: BindQuestions()
        /* Description: Binds questions from generic list to combo box
        /*********************************************************************************/
        private void BindQuestions()
        {
            cboQuestions.ItemsSource = null;

            questions.Sort((q1, q2) => q1.Text.CompareTo(q2.Text));
            cboQuestions.ItemsSource = questions;
        }

        /**********************************************************************************
        /* Name: BindQuestions() - first overload
        /* Description: Binds questions from generic list to combo box and selects a given question
        /* Parameters:
        /*   Question selectQuestion - Question to select in the combo box
        /*********************************************************************************/
        private void BindQuestions(Question selectQuestion)
        {
            BindQuestions();
            if (selectQuestion != null) cboQuestions.SelectedItem = selectQuestion;
        }

        /**********************************************************************************
        /* Name: LoadAndSelectQuestion()
        /* Description: Loads a single question from database and binds it to the combo box
        /* Parameters:
        /*   Question question - Question to select in the combo box
        /*********************************************************************************/
        private async void LoadAndSelectQuestion(Guid questionId)
        {
            try
            {
                question = await QuestionManager.LoadById(questionId);
                BindQuestions(question);
            }
            catch (Exception)
            {
                MessageBox.Show("Error loading question from the database.", "Error");
            }
        }

        /**********************************************************************************
        /* Name: cboQuestions_SelectionChanged()
        /* Description: Updates the form when changing combo box selection
        /*********************************************************************************/
        private void cboQuestions_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cboQuestions.SelectedIndex > -1) // Selected a question from the list
            {
                btnSave.IsEnabled = true;
                question = questions[cboQuestions.SelectedIndex]; // Set selected question
                foreach (ucQuestionAnswer field in AnswerFields)
                {
                    field.IsEnabled = true;
                    field.ClearAnswer(); // Clear previous selections
                }
                LoadFieldAnswers(); // Load answers for selected question
            }
            else // No question selected
            {
                btnSave.IsEnabled = false;
                question = null;
                foreach (ucQuestionAnswer field in AnswerFields) field.IsEnabled = false;
            }
        }

        /**********************************************************************************
        /* Name: btnEditAnswers_Click()
        /* Description: Displays the maintenance form and rebinds answers when complete
        /*********************************************************************************/
        private void btnEditAnswers_Click(object sender, RoutedEventArgs e)
        {
            new MaintainSurveyItem(answers).ShowDialog();
            LoadAndBindAnswers(); // Reload to keep DB and generic list in sync in case of a DB error
        }

        /**********************************************************************************
        /* Name: btnEditQuestions_Click()
        /* Description: Displays the maintanance form and rebinds questions when complete
        /*********************************************************************************/
        private void btnEditQuestions_Click(object sender, RoutedEventArgs e)
        {
            new MaintainSurveyItem(questions).ShowDialog();
            LoadAndBindQuestions(); // Reload to keep DB and generic list in sync in case of a DB error
        }

        /**********************************************************************************
        /* Name: btnSave_Click()
        /* Description: Saves changes to a questions's bound answers
        /*********************************************************************************/
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (question == null) return; // Should not happen if buttons are handled correctly

                if (PassedValidation())
                {
                    question.Answers = new List<Answer>(); // Clear out any answers previously bound to this question
                    foreach (ucQuestionAnswer field in AnswerFields)
                    {
                        if (field.SelectedAnswer != null) question.Answers.Add(new Answer // Add selected answers from screen
                        {
                            Id = (Guid)field.AnswerId,
                            IsCorrect = field.IsCorrect
                        });
                    }
                    Task task = Task.Run(async () =>
                    {
                        await QuestionAnswerManager.Insert(question); // Save to DB
                    });
                    task.Wait(); // Wait for task to complete in order to catch any exceptions
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Error saving question answers to the database.", "Error");

                // Reload question and associated answers from database to keep local list in sync
                LoadAndSelectQuestion(question.Id);
                foreach (ucQuestionAnswer field in AnswerFields) field.ClearAnswer();
                LoadFieldAnswers();
            }
        }

        /**********************************************************************************
        /* Name: PassedValidation()
        /* Description: Checks whether form passes validation checks prior to save. A question
        /*   must have 2 or more associated answers and one correct answer. It is ok to have
        /*   no associated answers.  Unlinking all answers before saving removes the question
        /*   from tblQuestionAnswer, which removes the foreign key constraint and allows the
        /*   question itself to be deleted.
        /* Returns: True if validation checks passed, false otherwise
        /*********************************************************************************/
        private bool PassedValidation()
        {
            int answersMarkedCorrect = 0;
            int numAnswers = 0;

            foreach (ucQuestionAnswer field in AnswerFields)
            {
                if (field.SelectedAnswer != null) numAnswers++;
                if (field.IsCorrect) answersMarkedCorrect++;
            }

            if (numAnswers == 1)
            {
                MessageBox.Show("Question must have at least 2 answers.");
                return false;
            }
            if (numAnswers > 1 && answersMarkedCorrect != 1)
            {
                MessageBox.Show("Question must have 1 correct answer.");
                return false;
            }

            return true;
        }
    }
}
