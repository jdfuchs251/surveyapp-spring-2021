﻿ALTER TABLE [dbo].[tblQuestionAnswer]
	ADD CONSTRAINT [tblQuestionAnswer_Answer]
	FOREIGN KEY (AnswerId)
	REFERENCES [tblAnswer] (Id)
