﻿ALTER TABLE [dbo].[tblResponse]
	ADD CONSTRAINT [tblResponse_Answer]
	FOREIGN KEY (AnswerId)
	REFERENCES [tblAnswer] (Id)
