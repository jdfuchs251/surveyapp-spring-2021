﻿begin
	/* Don't need to declare these again since this script is executed in the same batch as QuestionAnswers.sql
	-- Declare placeholders for IDs
	declare @question1 uniqueidentifier
	declare @question2 uniqueidentifier
	declare @question3 uniqueidentifier

	-- Get question IDs
	set @question1 = (select Id from tblQuestion where Question = 'Which of the following numbers is a perfect square?')
	set @question2 = (select Id from tblQuestion where Question = 'Which of the following numbers is a perfect cube?')
	set @question3 = (select Id from tblQuestion where Question = 'Which of the following is a triangular number?')
	*/

	-- Insert values into table
	insert into tblActivation (Id, QuestionId, StartDate, EndDate, ActivationCode)
	values
	(NEWID(), @question1, CONVERT(datetime, '2/1/2021'), CONVERT(datetime, '2/28/2021'), 'test01'),
	(NEWID(), @question2, CONVERT(datetime, '3/1/2021'), CONVERT(datetime, '3/3/2021'), 'test02'),
	(NEWID(), @question3, CONVERT(datetime, '3/4/2021'), CONVERT(datetime, '3/31/2021'), 'test03')
end