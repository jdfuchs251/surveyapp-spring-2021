﻿begin
	/* Don't need to declare these again since this script is executed in the same batch as QuestionAnswers.sql
	-- Declare placeholders for IDs
	declare @question1 uniqueidentifier
	declare @question2 uniqueidentifier
	declare @question3 uniqueidentifier
	declare @answer1 uniqueidentifier
	declare @answer2 uniqueidentifier
	declare @answer3 uniqueidentifier

	-- Get question IDs
	set @question1 = (select Id from tblQuestion where Question = 'Which of the following numbers is a perfect square?')
	set @question2 = (select Id from tblQuestion where Question = 'Which of the following numbers is a perfect cube?')
	set @question3 = (select Id from tblQuestion where Question = 'Which of the following is a triangular number?')

	-- Get answer IDs
	set @answer1 = (select Id from tblAnswer where Answer = '16')
	set @answer2 = (select Id from tblAnswer where Answer = '8')
	set @answer3 = (select Id from tblAnswer where Answer = '10')
	*/

	-- Insert values into table
	insert into tblResponse (Id, QuestionId, AnswerId, ResponseDate)
	values
	(NEWID(), @question1, @answer1, CONVERT(datetime, '2/15/2021')),
	(NEWID(), @question1, @answer2, CONVERT(datetime, '2/20/2021')),
	(NEWID(), @question2, @answer1, CONVERT(datetime, '3/2/2021')),
	(NEWID(), @question2, @answer2, CONVERT(datetime, '3/2/2021')),
	(NEWID(), @question3, @answer1, CONVERT(datetime, '3/3/2021')),
	(NEWID(), @question3, @answer3, CONVERT(datetime, '3/4/2021'))
end