﻿begin
	-- Declare placeholders for IDs
	declare @question1 uniqueidentifier
	declare @question2 uniqueidentifier
	declare @question3 uniqueidentifier
	declare @question4 uniqueidentifier
	declare @question5 uniqueidentifier
	declare @answer1 uniqueidentifier
	declare @answer2 uniqueidentifier
	declare @answer3 uniqueidentifier
	declare @answer4 uniqueidentifier
	declare @answer5 uniqueidentifier

	-- Get question IDs
	set @question1 = (select Id from tblQuestion where Question = 'Which of the following numbers is a perfect square?')
	set @question2 = (select Id from tblQuestion where Question = 'Which of the following numbers is a perfect cube?')
	set @question3 = (select Id from tblQuestion where Question = 'Which of the following is a triangular number?')
	set @question4 = (select Id from tblQuestion where Question = 'Which of the following is a member of the Fibonacci sequence?')
	set @question5 = (select Id from tblQuestion where Question = 'Which of the following numbers is prime?')

	-- Get answer IDs
	set @answer1 = (select Id from tblAnswer where Answer = '16')
	set @answer2 = (select Id from tblAnswer where Answer = '8')
	set @answer3 = (select Id from tblAnswer where Answer = '10')
	set @answer4 = (select Id from tblAnswer where Answer = '13')
	set @answer5 = (select Id from tblAnswer where Answer = '17')

	-- Insert values into table
	insert into tblQuestionAnswer (Id, QuestionId, AnswerId, IsCorrect)
	values
	(NEWID(), @question1, @answer2, 0),
	(NEWID(), @question1, @answer5, 0),
	(NEWID(), @question1, @answer1, 1),
	(NEWID(), @question2, @answer2, 1),
	(NEWID(), @question2, @answer4, 0),
	(NEWID(), @question2, @answer1, 0),
	(NEWID(), @question3, @answer3, 1),
	(NEWID(), @question3, @answer1, 0),
	(NEWID(), @question3, @answer4, 0),
	(NEWID(), @question4, @answer5, 0),
	(NEWID(), @question4, @answer4, 1),
	(NEWID(), @question4, @answer1, 0),
	(NEWID(), @question5, @answer5, 1),
	(NEWID(), @question5, @answer3, 0),
	(NEWID(), @question5, @answer2, 0)
end