﻿begin
	insert into tblQuestion (Id, Question)
	values
	(NEWID(), 'Which of the following numbers is a perfect square?'),
	(NEWID(), 'Which of the following numbers is a perfect cube?'),
	(NEWID(), 'Which of the following is a triangular number?'),
	(NEWID(), 'Which of the following is a member of the Fibonacci sequence?'),
	(NEWID(), 'Which of the following numbers is prime?')
end