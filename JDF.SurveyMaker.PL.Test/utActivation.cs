﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using JDF.SurveyMaker.PL;
using Microsoft.EntityFrameworkCore.Storage;
using System.Linq;
using System;

namespace JDF.SurveyMaker.PL.Test
{
    [TestClass]
    public class utActivation
    {
        protected SurveyEntities dc;
        protected IDbContextTransaction transaction;

        [TestInitialize]
        public void TestInitialize()
        {
            dc = new SurveyEntities();
            transaction = dc.Database.BeginTransaction();
        }

        [TestCleanup]
        public void TestCleanUp()
        {
            transaction.Rollback();
            transaction.Dispose();
            dc = null;
        }

        [TestMethod]
        public void LoadTest()
        {
            Assert.AreEqual(3, dc.tblActivations.Count());
        }

        [TestMethod]
        public void InsertTest()
        {
            tblActivation tblActivation = new tblActivation
            {
                Id = Guid.NewGuid(),
                QuestionId = dc.tblQuestions.FirstOrDefault().Id,
                StartDate = new DateTime(2021, 5, 1),
                EndDate = new DateTime(2021, 5, 31),
                ActivationCode = "test04"
            };

            dc.tblActivations.Add(tblActivation);
            int results = dc.SaveChanges();

            Assert.IsTrue(results > 0);
        }

        [TestMethod]
        public void UpdateTest()
        {
            int results = 0;
            InsertTest();

            tblActivation tblActivation = dc.tblActivations.FirstOrDefault(a => a.ActivationCode == "test04");
            if (tblActivation != null)
            {
                tblActivation.EndDate = new DateTime(2021, 6, 1);
                tblActivation.ActivationCode = "test05";

                results = dc.SaveChanges();
            }

            Assert.IsTrue(results > 0);
        }

        [TestMethod]
        public void DeleteTest()
        {
            int results = 0;
            InsertTest();

            tblActivation tblActivation = dc.tblActivations.FirstOrDefault(a => a.ActivationCode == "test04");
            if (tblActivation != null)
            {
                dc.tblActivations.Remove(tblActivation);
                results = dc.SaveChanges();
            }

            Assert.IsTrue(results > 0);
        }
    }
}
