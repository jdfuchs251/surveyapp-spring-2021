﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using JDF.SurveyMaker.PL;
using Microsoft.EntityFrameworkCore.Storage;
using System.Linq;
using System;

namespace JDF.SurveyMaker.PL.Test
{
    [TestClass]
    public class utAnswer
    {
        protected SurveyEntities dc;
        protected IDbContextTransaction transaction;

        [TestInitialize]
        public void TestInitialize()
        {
            dc = new SurveyEntities();
            transaction = dc.Database.BeginTransaction();
        }

        [TestCleanup]
        public void TestCleanUp()
        {
            transaction.Rollback();
            transaction.Dispose();
            dc = null;
        }

        [TestMethod]
        public void LoadTest()
        {
            Assert.AreEqual(5, dc.tblAnswers.Count());
        }

        [TestMethod]
        public void InsertTest()
        {
            tblAnswer newrow = new tblAnswer();
            newrow.Id = Guid.NewGuid();
            newrow.Answer = "New Answer";

            dc.tblAnswers.Add(newrow);
            int result = dc.SaveChanges();

            Assert.IsTrue(result > 0);
        }

        [TestMethod]
        public void UpdateTest()
        {
            int result = 0;

            InsertTest();

            tblAnswer row = dc.tblAnswers.FirstOrDefault(a => a.Answer == "New Answer");

            if (row != null)
            {
                row.Answer = "Updated Answer";
                result = dc.SaveChanges();
            }

            Assert.IsTrue(result > 0);
        }

        [TestMethod]
        public void DeleteTest()
        {
            int result = 0;

            InsertTest();

            tblAnswer row = dc.tblAnswers.FirstOrDefault(a => a.Answer == "New Answer");

            if (row != null)
            {
                dc.tblAnswers.Remove(row);
                result = dc.SaveChanges();
            }

            Assert.IsTrue(result > 0);
        }
    }
}
