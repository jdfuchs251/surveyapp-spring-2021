﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using JDF.SurveyMaker.PL;
using Microsoft.EntityFrameworkCore.Storage;
using System.Linq;
using System;

namespace JDF.SurveyMaker.PL.Test
{
    [TestClass]
    public class utResponse
    {
        protected SurveyEntities dc;
        protected IDbContextTransaction transaction;

        [TestInitialize]
        public void TestInitialize()
        {
            dc = new SurveyEntities();
            transaction = dc.Database.BeginTransaction();
        }

        [TestCleanup]
        public void TestCleanUp()
        {
            transaction.Rollback();
            transaction.Dispose();
            dc = null;
        }

        [TestMethod]
        public void LoadTest()
        {
            Assert.AreEqual(6, dc.tblResponses.Count());
        }

        [TestMethod]
        public void InsertTest()
        {
            tblResponse tblResponse = new tblResponse
            {
                Id = Guid.NewGuid(),
                QuestionId = dc.tblQuestions.FirstOrDefault().Id,
                AnswerId = dc.tblAnswers.FirstOrDefault().Id,
                ResponseDate = new DateTime(2021, 6, 1)
            };

            dc.tblResponses.Add(tblResponse);
            int results = dc.SaveChanges();

            Assert.IsTrue(results > 0);
        }

        [TestMethod]
        public void UpdateTest()
        {
            int results = 0;
            InsertTest();

            tblResponse tblResponse = dc.tblResponses.FirstOrDefault(r => r.ResponseDate == new DateTime(2021, 6, 1));
            if (tblResponse != null)
            {
                tblResponse.ResponseDate = new DateTime(2021, 6, 2);

                results = dc.SaveChanges();
            }

            Assert.IsTrue(results > 0);
        }

        [TestMethod]
        public void DeleteTest()
        {
            int results = 0;
            InsertTest();

            tblResponse tblResponse = dc.tblResponses.FirstOrDefault(r => r.ResponseDate == new DateTime(2021, 6, 1));
            if (tblResponse != null)
            {
                dc.tblResponses.Remove(tblResponse);
                results = dc.SaveChanges();
            }

            Assert.IsTrue(results > 0);
        }
    }
}
