﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using JDF.SurveyMaker.PL;
using Microsoft.EntityFrameworkCore.Storage;
using System.Linq;
using System;

namespace JDF.SurveyMaker.PL.Test
{
    [TestClass]
    public class utQuestionAnswer
    {
        protected SurveyEntities dc;
        protected IDbContextTransaction transaction;

        [TestInitialize]
        public void TestInitialize()
        {
            dc = new SurveyEntities();
            transaction = dc.Database.BeginTransaction();
        }

        [TestCleanup]
        public void TestCleanUp()
        {
            transaction.Rollback();
            transaction.Dispose();
            dc = null;
        }

        [TestMethod]
        public void LoadTest()
        {
            Assert.AreEqual(15, dc.tblQuestionAnswers.Count());
        }

        [TestMethod]
        public void InsertTest()
        {
            tblQuestionAnswer newrow = new tblQuestionAnswer();
            newrow.Id = Guid.NewGuid();
            newrow.QuestionId = dc.tblQuestions.FirstOrDefault().Id; // Take the first question
            newrow.AnswerId = dc.tblAnswers.FirstOrDefault().Id; // Take the first answer
            newrow.IsCorrect = false;

            dc.tblQuestionAnswers.Add(newrow);
            int result = dc.SaveChanges();

            Assert.IsTrue(result > 0);
        }

        [TestMethod]
        public void UpdateTest()
        {
            int result = 0;

            InsertTest();

            // Use the question and answer Ids we just inserted
            Guid qId = dc.tblQuestions.FirstOrDefault().Id;
            Guid aId = dc.tblAnswers.FirstOrDefault().Id;

            tblQuestionAnswer row = dc.tblQuestionAnswers.FirstOrDefault(qa => qa.QuestionId == qId && qa.AnswerId == aId);

            if (row != null)
            {
                row.IsCorrect = !row.IsCorrect;
                result = dc.SaveChanges();
            }

            Assert.IsTrue(result > 0);
        }

        [TestMethod]
        public void DeleteTest()
        {
            int result = 0;

            InsertTest();

            Guid qId = dc.tblQuestions.FirstOrDefault().Id;
            Guid aId = dc.tblAnswers.FirstOrDefault().Id;

            tblQuestionAnswer row = dc.tblQuestionAnswers.FirstOrDefault(qa => qa.QuestionId == qId && qa.AnswerId == aId);

            if (row != null)
            {
                dc.tblQuestionAnswers.Remove(row);
                result = dc.SaveChanges();
            }

            Assert.IsTrue(result > 0);
        }
    }
}