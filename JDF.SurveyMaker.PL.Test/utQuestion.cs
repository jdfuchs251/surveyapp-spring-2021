using Microsoft.VisualStudio.TestTools.UnitTesting;
using JDF.SurveyMaker.PL;
using Microsoft.EntityFrameworkCore.Storage;
using System.Linq;
using System;

namespace JDF.SurveyMaker.PL.Test
{
    [TestClass]
    public class utQuestion
    {
        protected SurveyEntities dc;
        protected IDbContextTransaction transaction;

        [TestInitialize]
        public void TestInitialize()
        {
            dc = new SurveyEntities();
            transaction = dc.Database.BeginTransaction();
        }

        [TestCleanup]
        public void TestCleanUp()
        {
            transaction.Rollback();
            transaction.Dispose();
            dc = null;
        }

        [TestMethod]
        public void LoadTest()
        {
            Assert.AreEqual(5, dc.tblQuestions.Count());
        }

        [TestMethod]
        public void InsertTest()
        {
            tblQuestion newrow = new tblQuestion();
            newrow.Id = Guid.NewGuid();
            newrow.Question = "New Question";

            dc.tblQuestions.Add(newrow);
            int result = dc.SaveChanges();

            Assert.IsTrue(result > 0);
        }

        [TestMethod]
        public void UpdateTest()
        {
            int result = 0;

            InsertTest();

            tblQuestion row = dc.tblQuestions.FirstOrDefault(q => q.Question == "New Question");

            if (row != null)
            {
                row.Question = "Updated Question";
                result = dc.SaveChanges();
            }

            Assert.IsTrue(result > 0);
        }

        [TestMethod]
        public void DeleteTest()
        {
            int result = 0;

            InsertTest();

            tblQuestion row = dc.tblQuestions.FirstOrDefault(q => q.Question == "New Question");

            if (row != null)
            {
                dc.tblQuestions.Remove(row);
                result = dc.SaveChanges();
            }

            Assert.IsTrue(result > 0);
        }
    }
}
