﻿using System;
using System.Collections.Generic;

#nullable disable

namespace JDF.SurveyMaker.PL
{
    public partial class tblQuestion
    {
        public tblQuestion()
        {
            tblActivations = new HashSet<tblActivation>();
            tblQuestionAnswers = new HashSet<tblQuestionAnswer>();
            tblResponses = new HashSet<tblResponse>();
        }

        public Guid Id { get; set; }
        public string Question { get; set; }

        public virtual ICollection<tblActivation> tblActivations { get; set; }
        public virtual ICollection<tblQuestionAnswer> tblQuestionAnswers { get; set; }
        public virtual ICollection<tblResponse> tblResponses { get; set; }
    }
}
