﻿using System;
using System.Collections.Generic;

#nullable disable

namespace JDF.SurveyMaker.PL
{
    public partial class tblAnswer
    {
        public tblAnswer()
        {
            tblQuestionAnswers = new HashSet<tblQuestionAnswer>();
            tblResponses = new HashSet<tblResponse>();
        }

        public Guid Id { get; set; }
        public string Answer { get; set; }

        public virtual ICollection<tblQuestionAnswer> tblQuestionAnswers { get; set; }
        public virtual ICollection<tblResponse> tblResponses { get; set; }
    }
}
